pre-commit-hooks
======================

pre-commit-hook to check jupyter notebooks prior to commit

## Installation as pre-commit hooks

See [pre-commit](https://github.com/pre-commit/pre-commit) for instructions

Sample `.pre-commit-config.yaml`

```yaml
-   repo: https://gitlab.com/Negusbuk/pre-commit-hooks.git
    rev: v1.0.1
    hooks:
    -   id: check-jupyter-notebook
```
