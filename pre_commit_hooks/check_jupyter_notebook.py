from __future__ import annotations

import argparse
import os
import subprocess

def checkFile(filename, clearOutput: bool = False) -> int:
	print("checking file {:s}".format(filename))
	with open(filename) as file:
		while line := file.readline().rstrip():
			if "execution_count" not in line:
				continue
			if "null" not in line:
				if clearOutput==False:
					print('{:s} not clean'.format(filename))
					return 1

				print('clearing output of {:s}'.format(filename))
				subprocess.run([
					'jupyter',
					'nbconvert',
					'--clear-output',
					'--inplace',
					filename
				])

	return 0

def main(argv: Sequence[str] | None = None) -> int:
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'filenames', 
		nargs='*', 
		help='Filenames to fix')
	parser.add_argument(
		'--clear',
		action='store_true',
		help='clear output of notebooks',
	)
	args = parser.parse_args(argv)

	ret = 0
	for filename in args.filenames:
		ret |= checkFile(filename, clearOutput=args.clear)
	return ret

if __name__ == '__main__':
	raise SystemExit(main())
